# Happy Piranha

Happy Salmon card game clone.

Find the current main branch deployed [here](https://bauerei.gitlab.io/happy-piranha).

## About the game

Shamelessly copied from the original card game description

> Happy Salmon is a quick party game for 3-8 players. Each player gets a stack of cards. Every card has a simple action that requires a partner. Players frantically shout their actions looking for matching players. When they find a match, they both complete the action and discard their used cards. The first player to finish all of their cards wins!
>
> --- https://www.explodingkittens.com/products/happy-salmon

As the game involves high fiving each other and running around, Happy Piranha is played best on a mobile device (unless you want to balance your laptop in one hand all the time - then feel free to do so).

## License
This project is licensed under [MIT](LICENSE)
