import 'deck.dart';

import 'package:collection/collection.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(const HappyPiranhaApp());
}

class HappyPiranhaApp extends StatelessWidget {
  const HappyPiranhaApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Happy Piranha',
      theme: ThemeData(
        useMaterial3: true,
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.red),
      ),
      home: const MainScreen(title: 'Happy Piranha'),
    );
  }
}

class MainScreen extends StatefulWidget {
  const MainScreen({super.key, required this.title});

  final String title;

  @override
  State<MainScreen> createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  Deck _deck = StartingDeck.defaultDeck();

  _MainScreenState();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      drawer: Drawer(
          child: ListView(children: [
        ListTile(
          leading: const Icon(Icons.restart_alt),
          title: const Text('Restart'),
          onTap: () {
            setState(() {
              _deck = _deck.reset();
            });
            // close drawer after tile was tapped
            Navigator.pop(context);
          },
        ),
      ])),
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: HPCardDeck(
            deck: _deck,
            onDelay: () {
              setState(() {
                var tmp = _deck.cards.removeAt(0);
                _deck.cards.add(tmp);
              });
            },
            onDismiss: () {
              setState(() {
                if (_deck.cards.isNotEmpty) {
                  _deck.cards.removeAt(0);
                }
              });
            },
            onTap: () {
              setState(() {
                _deck = PlayingDeck(_deck.cards);
              });
            }),
      ),
    );
  }
}

class HPCardDeck extends StatelessWidget {
  const HPCardDeck(
      {super.key,
      required this.deck,
      this.onDelay,
      this.onDismiss,
      this.onTap});

  final Deck deck;
  final void Function()? onDelay;
  final void Function()? onDismiss;
  final void Function()? onTap;
  final double offset = 5.0;
  final double width = 200.0;
  final double height = 300.0;

  @override
  Widget build(BuildContext context) {
    return Stack(
        clipBehavior: Clip.none,
        children: deck.cards.reversed.mapIndexed((index, element) {
          if (index == 0 && deck.cards.length == 1) {
            // Last card in the deck must always be not positioned, s.t. remaining cards are positioned to it
            // In this case, it is the last remaining card in the deck and must be dismissable
            return HPForegroundCard(
              title: element,
              width: width,
              height: height,
              onDelay: onDelay,
              onDismiss: onDismiss,
            );
          } else if (index == 0) {
            // More than 1 card remaining. This card is in the background and must not be dismissable
            return HPBackgroundCard(width: width, height: height);
          } else if (index == deck.cards.length - 1) {
            var topCard = deck is StartingDeck
                ? HPFaceDownCard(
                    width: width,
                    height: height,
                    onTap: onTap,
                  )
                : HPForegroundCard(
                    title: element,
                    width: width,
                    height: height,
                    onDelay: onDelay,
                    onDismiss: onDismiss,
                  );
            // Upmost card. Must be dismissable
            return Positioned(
              top: index * offset,
              left: index * offset,
              child: topCard,
            );
          } else {
            // All other cards are in the background and must not be dismissable. They are positioned according to the last card
            return Positioned(
              top: index * offset,
              left: index * offset,
              child: HPBackgroundCard(width: width, height: height),
            );
          }
        }).toList());
  }
}

class HPFaceDownCard extends StatelessWidget {
  const HPFaceDownCard(
      {super.key, required this.width, required this.height, this.onTap});

  final double width;
  final double height;
  final void Function()? onTap;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);

    return GestureDetector(
      onTap: () {
        onTap?.call();
      },
      child: Card(
        elevation: 0,
        color: Colors.white,
        shape: RoundedRectangleBorder(
          side: BorderSide(
            color: theme.colorScheme.primary,
            width: 2.0,
            strokeAlign: -5.0,
          ),
          borderRadius: const BorderRadius.all(Radius.circular(12)),
        ),
        child: Padding(
            padding: const EdgeInsets.all(2.0),
            child: SizedBox(width: width, height: height)),
      ),
    );
  }
}

class HPBackgroundCard extends StatelessWidget {
  const HPBackgroundCard(
      {super.key, required this.width, required this.height});

  final double width;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 1,
      child: SizedBox(width: width, height: height),
    );
  }
}

class HPForegroundCard extends StatelessWidget {
  const HPForegroundCard(
      {super.key,
      required this.title,
      required this.width,
      required this.height,
      this.onDelay,
      this.onDismiss});

  final String title;
  final double width;
  final double height;
  final void Function()? onDelay;
  final void Function()? onDismiss;

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    final titleStyle = theme.textTheme.titleLarge!.copyWith(
      color: theme.colorScheme.primary,
      fontWeight: FontWeight.bold,
    );
    return Dismissible(
        key: UniqueKey(),
        background: Container(color: Colors.lightBlue),
        secondaryBackground: Container(color: Colors.red),
        onDismissed: (direction) {
          switch (direction) {
            case DismissDirection.endToStart:
              // swipe left
              onDelay?.call();
              break;
            case DismissDirection.startToEnd:
              // swipe right
              onDismiss?.call();
              break;
            default:
              // Do nothing
              break;
          }
        },
        child: Card(
          elevation: 0,
          color: Colors.white,
          shape: RoundedRectangleBorder(
            side: BorderSide(
              color: theme.colorScheme.primary,
              width: 2.0,
              strokeAlign: -5.0,
            ),
            borderRadius: const BorderRadius.all(Radius.circular(12)),
          ),
          child: Padding(
            padding: const EdgeInsets.all(2.0),
            child: SizedBox(
              width: width,
              height: height,
              child: Column(
                children: [
                  Expanded(
                      child: Center(child: Text(title, style: titleStyle))),
                  const Expanded(
                      child: Padding(
                    padding: EdgeInsets.all(8.0),
                    child: Placeholder(),
                  )),
                ],
              ),
            ),
          ),
        ));
  }
}
