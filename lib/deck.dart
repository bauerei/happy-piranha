abstract class Deck {
  Deck(this.cards);

  List<String> cards;

  Deck reset();
}

class StartingDeck extends Deck {
  static List<String> _shuffeledDefaultCards() {
    const defaultCards = [
      "high five",
      "high five",
      "high five",
      "fish bump",
      "fish bump",
      "fish bump",
      "happy salmon",
      "happy salmon",
      "happy salmon",
      "switch it up",
      "switch it up",
      "switch it up",
    ];
    List<String> defaultCardsCopy = List.from(defaultCards);
    defaultCardsCopy.shuffle();

    return defaultCardsCopy;
  }

  StartingDeck(super.cards);

  StartingDeck.defaultDeck() : super(_shuffeledDefaultCards());

  @override
  Deck reset() {
    return this;
  }
}

class PlayingDeck extends Deck {
  PlayingDeck(super.cards);

  @override
  Deck reset() {
    return StartingDeck.defaultDeck();
  }
}
